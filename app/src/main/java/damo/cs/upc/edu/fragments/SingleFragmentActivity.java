package damo.cs.upc.edu.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.view.Menu;
import android.view.MenuItem;


/**
 * Created by josepm on 28/6/16.
 */
public abstract class SingleFragmentActivity extends Activity
        implements FragmentObservable.ObservadorFragment {

    private Fragment f;

    @LayoutRes
    protected abstract int getLayoutResId();

    @IdRes
    protected abstract int getContenidorFragmentResId();

    protected abstract Fragment getInstance();

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());

        FragmentManager fm = getFragmentManager();

        f = fm.findFragmentById(getContenidorFragmentResId());

        if (f == null) {
            f = getInstance();
            fm.beginTransaction()
                    .add(getContenidorFragmentResId(), f)
                    .commit();

        }

        // En la recreació de l'activity el fragmentManager reconstrueix els fragments. I per
        // això no cal tornar-lo a crear. Però com que l'activitat és nova cal canviar l'observador
        // del fragment hostatjat. Altrament avisaríem a una activitat que ja no existeix.

        ((FragmentObservable) f).setObservadorFragment(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hola_android, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

