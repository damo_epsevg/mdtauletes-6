/**
 * HOLA ANDROID
 */


package damo.cs.upc.edu.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import java.util.UUID;

public class CrimActivity extends SingleFragmentActivity {


    private static final String IDCRIM = "id_crim";

    public static final Intent getIntent(Activity a, UUID id) {
        Intent intent = new Intent(a, CrimActivity.class);
        intent.putExtra(IDCRIM, id);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Fragment getInstance() {
        UUID idCrim = (UUID) getIntent().getSerializableExtra(IDCRIM);
        return CrimFragment.getInstance(idCrim);
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.un_panell_activity;
    }

    @Override
    protected int getContenidorFragmentResId() {
        return R.id.contenidor;
    }


    @Override
    public void onCanviFragment(Crim crim, int idCrida) {

    }
}
